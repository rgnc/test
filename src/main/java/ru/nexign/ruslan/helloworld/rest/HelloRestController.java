package ru.nexign.ruslan.helloworld.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class HelloRestController {

    @GetMapping
    public String get() {
        return "Hello World at " + LocalDateTime.now();
    }

}